package br.com.newm.controllers;

import com.orm.query.Condition;
import com.orm.query.Select;

import java.util.List;

import br.com.newm.models.Cliente;

/**
 * Criado por Felipe Samuel em 30/01/2019.
 */
public class ClienteController {

    public List<Cliente> getClientes(){
        return Select.from(Cliente.class).orderBy("nome ASC").list();
    }

    public long getSizeClientes(){
        return Select.from(Cliente.class).count();
    }

    public List<Cliente> getClientes(String filtro){
        return Select.from(Cliente.class)
                .whereOr(Condition.prop("nome").like("%"+filtro+"%"))
                .whereOr(Condition.prop("celular").like("%"+filtro+"%"))
                .whereOr(Condition.prop("email").like("%"+filtro+"%"))
                .whereOr(Condition.prop("cpf").like("%"+filtro+"%"))
                .whereOr(Condition.prop("nascimento").like("%"+filtro+"%"))
                .whereOr(Condition.prop("endereco").like("%"+filtro+"%"))
                .whereOr(Condition.prop("observacao").like("%"+filtro+"%"))
                .orderBy("nome ASC").list();
    }
}
