package br.com.newm.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.futuremind.recyclerviewfastscroll.SectionTitleProvider;

import java.util.List;

import br.com.newm.R;
import br.com.newm.models.Cliente;

public class ClienteAdapter extends RecyclerView.Adapter<ClienteAdapter.ClienteViewHolder>
        implements SectionTitleProvider{

    private List<Cliente> clientes;

    public ClienteAdapter(List<Cliente> cliente) {
        clientes = cliente;
    }

    @Override
    public ClienteViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        return new ClienteViewHolder(LayoutInflater.from(parent.getContext())
                .inflate(R.layout.cliente_item, parent, false));
    }

    @Override
    public void onBindViewHolder(ClienteViewHolder holder, int position) {
        Cliente cliente = clientes.get(position);
        holder.tvNome.setText(cliente.getNome());
        holder.tvEmail.setText(cliente.getEmail());
        holder.tvDataNascimento.setText(cliente.getNascimento());
    }

    public void atualizarDataSet(List<Cliente> clientes) {
        this.clientes.clear();
        this.clientes.addAll(clientes);
        this.notifyDataSetChanged();
    }

    @Override
    public String getSectionTitle(int position) {
        return clientes.get(position).getNome().substring(0, 1);
    }

    @Override
    public int getItemCount() {
        return clientes != null ? clientes.size() : 0;
    }

    class ClienteViewHolder extends RecyclerView.ViewHolder{

        public ImageView ivFoto;
        public TextView tvNome;
        public TextView tvEmail;
        public TextView tvDataNascimento;

        public ClienteViewHolder(View itemView) {
            super(itemView);
            tvNome = itemView.findViewById(R.id.tvNome);
            ivFoto = itemView.findViewById(R.id.ivFoto);
            tvEmail = itemView.findViewById(R.id.tvEmail);
            tvDataNascimento = itemView.findViewById(R.id.tvDataNascimento);
        }

    }

}
