package br.com.newm.views;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.futuremind.recyclerviewfastscroll.FastScroller;
import com.miguelcatalan.materialsearchview.MaterialSearchView;

import java.util.ArrayList;
import java.util.List;

import br.com.newm.R;
import br.com.newm.adapters.ClienteAdapter;
import br.com.newm.controllers.ClienteController;
import br.com.newm.listeners.RecyclerClickListener;
import br.com.newm.models.Cliente;

public class MainActivity extends AppCompatActivity implements
        MaterialSearchView.OnQueryTextListener,
        MaterialSearchView.SearchViewListener,
        SwipeRefreshLayout.OnRefreshListener {


    private Toolbar toolbar;
    private FloatingActionButton fabNovoCliente;
    private MaterialSearchView searchView;
    private SwipeRefreshLayout swipeClientes;
    private RelativeLayout rlClientes;
    private LinearLayout llNenhumClienteCadastrado;

    private RecyclerView rvClientes;
    private FastScroller fastScroller;

    private ClienteAdapter adapter;
    private List<Cliente> clientes = new ArrayList<>();
    private ClienteController clienteController = new ClienteController();
    private int REQUEST_CODE_CLIENTE = 1;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        init();
    }

    private void init(){
        toolbar = findViewById(R.id.toolbar);
        fabNovoCliente = findViewById(R.id.fabNovoCliente);
        searchView = findViewById(R.id.msvPesquisa);
        rvClientes = findViewById(R.id.rvClientes);
        llNenhumClienteCadastrado = findViewById(R.id.llNenhumClienteCadastrado);
        rlClientes = findViewById(R.id.rlClientes);
        fastScroller = findViewById(R.id.fastscroll);
        swipeClientes = findViewById(R.id.swipeClientes);
        swipeClientes.setColorSchemeResources(R.color.colorPrimary,
                R.color.colorAccent,
                R.color.colorPrimaryDark);

        setSupportActionBar(toolbar);
        this.searchView.setOnQueryTextListener(this);
        this.searchView.setOnSearchViewListener(this);

        temCliente();
        initListeners();
        initList();
    }

    private void initList() {
        this.clientes = clienteController.getClientes();
        adapter = new ClienteAdapter(this.clientes);
        LinearLayoutManager layoutManager = new LinearLayoutManager(this);
        rvClientes.setLayoutManager(layoutManager);
        rvClientes.setAdapter(adapter);
        fastScroller.setRecyclerView(rvClientes);

    }

    private void initListeners(){
        fabNovoCliente.setOnClickListener(v -> {
            startActivityForResult(new Intent(this, ClienteActivity.class), REQUEST_CODE_CLIENTE);
        });

        swipeClientes.setOnRefreshListener(this);
        rvClientes.addOnItemTouchListener(new RecyclerClickListener(this, new RecyclerClickListener.OnItemClickListener() {
            @Override
            public void onItemClick(View view, int position) {
                Intent intent = new Intent(MainActivity.this, ClienteActivity.class);
                Bundle b = new Bundle();
                b.putLong("id", clientes.get(position).getId());
                intent.putExtras(b);
                startActivityForResult(intent, REQUEST_CODE_CLIENTE);
            }

            @Override
            public void onLongItemClick(View view, int position) {

            }
        }));

    }

    private void temCliente(){
        if(clienteController.getSizeClientes()==0){
            llNenhumClienteCadastrado.setVisibility(View.VISIBLE);
            rlClientes.setVisibility(View.INVISIBLE);
        }else{
            llNenhumClienteCadastrado.setVisibility(View.INVISIBLE);
            rlClientes.setVisibility(View.VISIBLE);
        }
    }

    private void sair(){
        new AlertDialog.Builder(this)
                .setMessage("Você tem certeza que deseja sair do aplicativo?")
                .setCancelable(false)
                .setPositiveButton("Sim", (dialog, id) -> {
                    finish();
                })
                .setNegativeButton("Não", null)
                .show();
    }

    @Override
    public void onBackPressed() {
        if (searchView.isSearchOpen()) {
            searchView.closeSearch();
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_main, menu);
        MenuItem item = menu.findItem(R.id.menu_pesquisar);
        searchView.setMenuItem(item);
        return true;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if (requestCode == REQUEST_CODE_CLIENTE) {
            if (resultCode == RESULT_OK) {
                adapter.atualizarDataSet(clienteController.getClientes());
                temCliente();
            }
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();

        if (id == R.id.menu_sair) {
            sair();
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        adapter.atualizarDataSet(clienteController.getClientes(newText));
        temCliente();
        return false;
    }

    @Override
    public void onSearchViewShown() {}

    @Override
    public void onSearchViewClosed() {}

    @Override
    public void onRefresh() {
        adapter.atualizarDataSet(clienteController.getClientes());
        temCliente();
        swipeClientes.setRefreshing(false);
    }
}
