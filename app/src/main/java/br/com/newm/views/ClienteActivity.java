package br.com.newm.views;

import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.mobsandgeeks.saripaar.ValidationError;
import com.mobsandgeeks.saripaar.Validator;
import com.mobsandgeeks.saripaar.annotation.Email;
import com.mobsandgeeks.saripaar.annotation.Length;
import com.mobsandgeeks.saripaar.annotation.NotEmpty;
import com.mobsandgeeks.saripaar.annotation.Pattern;
import com.orm.query.Condition;
import com.orm.query.Select;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;
import java.util.Locale;

import br.com.newm.R;
import br.com.newm.models.Cliente;
import br.com.newm.util.CpfUtil;
import br.com.newm.util.Global;
import br.com.newm.util.Mask;

public class ClienteActivity extends AppCompatActivity implements Validator.ValidationListener {

    Toolbar toolbar;

    @NotEmpty(message = "O campo nome é obrigatório")
    EditText etNome;

    @NotEmpty(message = "O campo celular é obrigatório")
    EditText etCelular;

    EditText etEmail;


    EditText etCpf;

    @NotEmpty(message = "O campo data de nascimento é obrigatório")
    EditText etDataNascimento;

    @NotEmpty(message = "O campo endereço é obrigatório")
    EditText etEndereco;


    @Length(max = 300, message = "A quantidade máxima é de 300 caracteres")
    EditText etObservacao;

    private Validator validator;
    private Cliente cliente;
    private Calendar meuCalendario = Calendar.getInstance();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_cliente);

        init();
        initParams();
    }


    private void initParams(){
        Bundle bundle = getIntent().getExtras();
        if(bundle != null) {
            long id = bundle.getLong("id");
            cliente = Select.from(Cliente.class)
                    .where(Condition.prop("id").eq(String.valueOf(id)))
                    .first();

            etNome.setText(cliente.getNome());
            etCelular.setText(cliente.getCelular());
            etEmail.setText(cliente.getEmail());
            etDataNascimento.setText(cliente.getNascimento());
            etCpf.setText(cliente.getCpf());
            etEndereco.setText(cliente.getEndereco());
            etObservacao.setText(cliente.getObservacao());
            getSupportActionBar().setTitle("Editar cliente");
        }
    }

    private void init() {
        validator = new Validator(this);
        validator.setValidationListener(this);
        toolbar = findViewById(R.id.toolbar);
        toolbar.setTitle("Adicionar cliente");

        etNome = findViewById(R.id.etNome);

        etCelular = findViewById(R.id.etCelular);
        etCelular.addTextChangedListener(Mask.mask(etCelular, Mask.FORMATO_CELULAR));

        etEmail = findViewById(R.id.etEmail);

        etCpf = findViewById(R.id.etCpf);
        etCpf.addTextChangedListener(Mask.mask(etCpf, Mask.FORMATO_CPF));

        etDataNascimento = findViewById(R.id.etNascimento);
        etDataNascimento.addTextChangedListener(Mask.mask(etDataNascimento, Mask.FORMATO_DATA));
        DatePickerDialog.OnDateSetListener date = (view, year, monthOfYear, dayOfMonth) -> {
            meuCalendario.set(Calendar.YEAR, year);
            meuCalendario.set(Calendar.MONTH, monthOfYear);
            meuCalendario.set(Calendar.DAY_OF_MONTH, dayOfMonth);
            atualizarData();
        };
        etDataNascimento.setOnFocusChangeListener((view, b) -> {
            if(b){
                new DatePickerDialog(ClienteActivity.this, date, meuCalendario
                        .get(Calendar.YEAR), meuCalendario.get(Calendar.MONTH),
                        meuCalendario.get(Calendar.DAY_OF_MONTH)).show();
            }
        });

        etEndereco = findViewById(R.id.etEndereco);
        etObservacao = findViewById(R.id.etObservacao);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

    }

    private void atualizarData() {
        String formato = "dd/MM/yyyy";
        SimpleDateFormat sdf = new SimpleDateFormat(formato, Locale.US);
        etDataNascimento.setText(sdf.format(meuCalendario.getTime()));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_cliente, menu);
        MenuItem item = menu.findItem(R.id.menu_excluir);

        if(cliente==null || cliente.getId()==0) {
            item.setVisible(false);
        }

        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()){
            case android.R.id.home:
                setResult(RESULT_OK);
                finish();
                break;
            case R.id.menu_salvar:
                Global.esconderTeclado(this);
                validator.validate();
                break;
            case R.id.menu_excluir:
                excluirCliente();
                break;

        }

        return super.onOptionsItemSelected(item);
    }

    private void excluirCliente(){
        new AlertDialog.Builder(this)
                .setMessage("Você tem certeza que deseja excluir o cliente?")
                .setCancelable(false)
                .setPositiveButton("Sim", (dialog, id) -> {
                    cliente.delete();
                    Toast.makeText(getApplicationContext(), "Cliente excluído com sucesso!", Toast.LENGTH_SHORT).show();
                    dialog.dismiss();
                    setResult(RESULT_OK);
                    finish();
                })
                .setNegativeButton("Não", null)
                .show();
    }

    @Override
    public void onValidationSucceeded() {

      if(isCelularValido()) {

                if (cliente == null || cliente.getId() == 0) {
                    cliente = new Cliente();
                }
                cliente.setNome(etNome.getText().toString());
                cliente.setCelular(etCelular.getText().toString());
                cliente.setEmail(etEmail.getText().toString());
                cliente.setCpf(etCpf.getText().toString());
                cliente.setEndereco(etEndereco.getText().toString());
                cliente.setObservacao(etObservacao.getText().toString());
                cliente.setNascimento(etDataNascimento.getText().toString());
                cliente.save();

                Toast.makeText(getApplicationContext(), "Cliente salvo com sucesso!", Toast.LENGTH_SHORT).show();
                setResult(RESULT_OK);
                finish();
            }else{
                etCelular.setError("O número do celular informado não é válido");
            }

    }
    @Override
    public void onValidationFailed(List<ValidationError> errors) {
        for (ValidationError error : errors) {
            View view = error.getView();
            String message = error.getCollatedErrorMessage(this);

            if (view instanceof EditText) {
                ((EditText) view).setError(message);
            } else {
                Toast.makeText(this, message, Toast.LENGTH_LONG).show();
            }
        }
    }
    private boolean isCelularValido(){

        String celularNumeros = etCelular.getText().toString().replaceAll("[^0-9]", "");
        String digito = Character.toString(celularNumeros.charAt(2));
        if(celularNumeros.length()==11 && digito.equals("9")){
            return true;
        }else {
            return false;
        }
    }
}
